# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository is for coding kata of Bowling Game.
Original description can be found here: http://codingdojo.org/kata/Bowling/
It has however been slightly modified already by:
-> validating impossible rolls (eg 9 nines in one frame)
New modifications are possible in the future versions.

### How do I get set up? ###

This project depends only on JUnit v4.
Running attached or writing own unit tests is also the preferred way for interacting with the code for now.

### Who do I talk to? ###

AdamczykTM@gmail.com