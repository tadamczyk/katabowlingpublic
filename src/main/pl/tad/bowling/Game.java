package main.pl.tad.bowling;

public interface Game {
    void roll(int pins);
    int score();
}
