package main.pl.tad.bowling.impl;

import java.util.ArrayList;
import java.util.List;

import main.pl.tad.bowling.Frame;
import main.pl.tad.bowling.exceptions.InvalidRollException;

class FrameImpl implements Frame {
    private final int frameNumber;
    private List<Integer> scores = new ArrayList<>();
    FrameType frameType = FrameType.NORMAL;
    private int strikeDistance;

    FrameImpl(int frameNumber) {
        this.frameNumber = frameNumber;
    }

    void doThrow(int score) {
        validateFutureRoll(score);
        scores.add(score);
        updateFrameType();
    }

    FrameImpl getNextFrame() {
        int nextFrameNumber = frameNumber + 1;
        return nextFrameNumber == MAX_FRAMES ? new LastFrameImpl(nextFrameNumber) : new FrameImpl(nextFrameNumber);
    }

    private void updateFrameType() {
        if (isFrameScoreEqualLimit() && scores.size() == 1) {
            frameType = FrameType.STRIKE;
        } else if (isFrameScoreEqualLimit() && scores.size() == 2) {
            frameType = FrameType.SPARE;
        }
    }

    protected void validateFutureRoll(int score) {
        if (!isValidWithPreviousScore(score)) {
            throw new InvalidRollException("Cannot throw more than " + MAX_FRAME_SCORE + " in a FrameImpl");
        }
    }

    boolean canThrow() {
        return lessThanNumberOfRolls(2) && isFrameScoreBelowLimit() && frameNumber <= MAX_FRAMES;
    }

    boolean isValidWithPreviousScore(int score) {
        return getLastScore() == MAX_FRAME_SCORE || (getLastScore() + score <= MAX_FRAME_SCORE);
    }

    private int getLastScore() {
        if (lessThanNumberOfRolls(1)) {
            return 0;
        }
        return scores.get(scores.size() - 1);
    }

    private boolean isFrameScoreBelowLimit() {
        return getFrameScore() < MAX_FRAME_SCORE;
    }

    private boolean isFrameScoreEqualLimit() {
        return getFrameScore() == MAX_FRAME_SCORE;
    }

    int getFrameScore() {
        return scores.stream()
                .mapToInt(Integer::intValue)
                .sum();
    }

    void applyBonusPoint(int score) {
        scores.add(score);
        ++strikeDistance;
    }

    boolean isActiveSpecialFrame() {
        return (frameType == FrameType.SPARE && strikeDistance < 1) || (frameType == FrameType.STRIKE && strikeDistance < 2);
    }

    boolean lessThanNumberOfRolls(int numberOfRolls){
        return scores.size() < numberOfRolls;
    }

}
