package main.pl.tad.bowling.impl;

import java.util.ArrayList;
import java.util.Collection;

import main.pl.tad.bowling.Frame;
import main.pl.tad.bowling.Game;

public class GameImpl implements Game {
    private FrameImpl currentFrame = new FrameImpl(1);
    private Collection<FrameImpl> frames = new ArrayList<>();

    {
        frames.add(currentFrame);
    }

    @Override
    public void roll(int pins) {
        if (shouldInitializeNewFrame()) {
            startNewFrame();
        }
        currentFrame.doThrow(pins);
        applyBonusPoints(pins);
    }

    private void applyBonusPoints(int pins) {
        frames.stream()
                .filter(FrameImpl::isActiveSpecialFrame)
                .filter(frame -> !frame.equals(currentFrame))
                .forEach(frame -> frame.applyBonusPoint(pins));
    }

    private void startNewFrame() {
        currentFrame = currentFrame.getNextFrame();
        frames.add(currentFrame);
    }

    private boolean shouldInitializeNewFrame() {
        return !currentFrame.canThrow() && frames.size() < Frame.MAX_FRAMES;
    }

    @Override
    public int score() {
        return frames.stream()
                .map(FrameImpl::getFrameScore)
                .mapToInt(Integer::intValue)
                .sum();
    }

}
