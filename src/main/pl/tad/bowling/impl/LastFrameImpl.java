package main.pl.tad.bowling.impl;

import main.pl.tad.bowling.exceptions.InvalidRollException;

public class LastFrameImpl extends FrameImpl {
    LastFrameImpl(int frameNumber) {
        super(frameNumber);
    }

    protected void validateFutureRoll(int score) {
        boolean isValid = true;
        if (lessThanNumberOfRolls(2)) {
            super.validateFutureRoll(score);
        } else {
            isValid = (frameType.equals(FrameType.STRIKE) && lessThanNumberOfRolls(3) && isValidWithPreviousScore(score)) || (frameType.equals(FrameType.SPARE) && lessThanNumberOfRolls(3));
        }
        if (!isValid) {
            throw new InvalidRollException("Cannot throw more than " + MAX_FRAME_SCORE + " in a FrameImpl");
        }
    }

    boolean canThrow() {
        return true;
    }
}
