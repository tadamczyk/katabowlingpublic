package main.pl.tad.bowling;

public interface Frame {
    int MAX_FRAME_SCORE = 10;
    int MAX_FRAMES = 10;

    enum FrameType {
        NORMAL, STRIKE, SPARE
    }
}
