package main.pl.tad.bowling.exceptions;

public class InvalidRollException extends RuntimeException {
    public InvalidRollException(String message) {
        super(message);
    }
}
