package test.pl.tad.bowling;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import main.pl.tad.bowling.Game;
import main.pl.tad.bowling.exceptions.InvalidRollException;
import main.pl.tad.bowling.impl.GameImpl;

public class GameTest {

    @Test
    public void scoreZeros() {
        Game game = repeatableRoll(0, 10);
        Assert.assertEquals(0, game.score());
    }

    @Test
    public void scoreFullButBreakBeforeLastFrame() {
        Game game = repeatableRoll(10, 10);
        Assert.assertEquals(270, game.score());
    }

    @Test
    public void scoreFull() {
        Game game = repeatableRoll(10, 12);
        Assert.assertEquals(300, game.score());
    }

    @Test(expected = InvalidRollException.class)
    public void scoreOverFull() {
        repeatableRoll(10, 13);
    }

    @Test(expected = InvalidRollException.class)
    public void scoreOverFrameLimit() {
        Game game = new GameImpl();
        game.roll(5);
        game.roll(6);
    }

    @Test(expected = InvalidRollException.class)
    public void scoreOverFrameLimitLastFrameStrike() {
        Game game = repeatableRoll(4, 18);
        Assert.assertEquals(18 * 4, game.score());
        game.roll(10);
        Assert.assertEquals(18 * 4 + 10, game.score());
        game.roll(4);
        Assert.assertEquals(19 * 4 + 10, game.score());
        game.roll(7);
    }

    @Test(expected = InvalidRollException.class)
    public void scoreOverFrameLimitLastFrameNormal() {
        Game game = repeatableRoll(4, 19);
        Assert.assertEquals(19 * 4, game.score());
        game.roll(8);
        Assert.assertEquals(19 * 4 + 8, game.score());
    }

    @Test
    public void lastFrameSpare() {
        Game game = repeatableRoll(4, 19);
        Assert.assertEquals(19 * 4, game.score());
        game.roll(6);
        Assert.assertEquals(19 * 4 + 6, game.score());
        game.roll(10);
        Assert.assertEquals(19 * 4 + 16, game.score());
    }

    @Test
    public void scoreAllNines() {
        Game game = repeatableRoll(9, 0, 20);
        Assert.assertEquals(90, game.score());
    }

    @Test
    public void scoreAllNinesButLast10() {
        Game game = repeatableRoll(9, 0, 18);
        game.roll(10);
        game.roll(0);
        Assert.assertEquals(91, game.score());
    }

    @Test
    public void realLifeScenario() {
        Game game = iterateGame(Arrays.asList(3, 5, 1, 2, 5, 2, 10, 6, 4, 1, 9, 10, 10, 6, 4, 8, 1));
        Assert.assertEquals(142, game.score());
    }

    private Game repeatableRoll(int pins, int tries) {
        return repeatableRoll(pins, pins, tries);
    }

    private Game repeatableRoll(int pinsOdd, int pinsEven, int tries) {
        Game game = new GameImpl();
        for (int i = 1; i < tries + 1; i++) {
            if (i % 2 == 0) {
                game.roll(pinsEven);
            } else {
                game.roll(pinsOdd);
            }
        }
        return game;
    }

    private Game iterateGame(List<Integer> scores) {
        Game game = new GameImpl();
        for (Integer score : scores) {
            game.roll(score);
        }
        return game;
    }

}
